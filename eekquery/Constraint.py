class Constraint:
    
    def __init__(self, *, sourceKeyName=None, targetColumnName=None, comparisonOperator='=',
        operand=None):

        if sourceKeyName is None and targetColumnName is not None:
            sourceKeyName = targetColumnName

        # A key into the entity argument.
        self.sourceKeyName = sourceKeyName
        # The name of the column being compared against.
        self.targetColumnName = targetColumnName
        # The comparison operator.
        self.comparisonOperator = comparisonOperator
        # The value to constrain by.
        self.operand = operand
        
    def __str__(self):
        return '{}, {} {} {}'.format(self.sourceKeyName, self.targetColumnName, self.comparisonOperator, self.operand)