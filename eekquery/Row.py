'''
Generic class representing a row in a table.
'''

class Row:
    
    def __init__(self, row):
        if isinstance(row, Row):
            self._rowDict = row._rowDict
        else:
            self._rowDict = row

    def getByColumnName(self, col):
        '''
        Returns the value in the given column for this row.
        '''
        return self._rowDict[col]          

    def getValues(self):
        '''
        Returns a list of values for each column in the row.
        '''
        return list(self._rowDict.values())

    def getColumns(self):
        '''
        Returns a string description of this row's columns.
        Used for building SQL queries.
        '''
        commaSeparatedKeys = ','.join(self._rowDict.keys()) 
        
        return '({})'.format(commaSeparatedKeys)  

    def getUpdates(self):
        '''
        Returns a list of (column name, value) pairs.
        '''

        return self._rowDict.items()
