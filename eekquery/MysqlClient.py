import datetime
from decimal import Decimal
import logging
import pymysql
from eekquery.Row import *
from eekquery.RowHook import RowHook
from eekquery.WhereClauseBuilder import WhereClauseBuilder


class MysqlClient:
    '''
    Encapsulates logic for interacting with a Mysql database.
    '''

    class QueryError(RuntimeError):
        pass

    _logger = logging.getLogger()

    def __init__(self, **kwargs):
        self._config = kwargs.get('config')
        self._database = None
        self.open()
        
        self._rowHooks = []

    def open(self):
        self._database = pymysql.connect(
            self._config.endpoint,
            user=self._config.username,
            passwd=self._config.password,
            db=self._config.name,
            autocommit=True,
            connect_timeout=5,
            charset=self._config.charset)
        
    def isClosed(self):
        return not self._database.open;

    def close(self):
        self._database.close()

    def beginTransaction(self):
        '''
        Starts a new database transaction.
        '''
        self._database.autocommit = False

    def commitTransaction(self):
        '''
        Commits the current transaction and turns autocommit back on.
        '''
        self._database.commit()
        self._database.autocommit = True

    @staticmethod
    def toJSONSerializable(package):
        '''
        Converts a dict's fields to be JSON serializable.
        In particular, converts dates to integer timestamps,
        and converts Decimal to float.
        '''
        result = dict()

        for field in package:
            if type(package[field]) == datetime.date:
                result[field] = str(package[field])
            elif isinstance(package[field], datetime.datetime):
                # http://www.psf.upfronthosting.co.za/issue31212
                try:
                    result[field] = int(package[field].timestamp() * 1000)
                except ValueError:
                    result[field] = None
            elif type(package[field]) == Decimal:
                result[field] = float(package[field])
            else:
                result[field] = package[field]

        return result

    def writeRow(self, rowDict, table, *, overwrite=True, **kwargs):
        '''
        Writes a row to the given table.
        Updates the row if it exists (by primary key), else inserts a new row.

        rowDict - A dictionary mapping column names to values.
        table - A table from MysqlClient.TABLES.
        '''
        
        row = Row(rowDict)
        columns = row.getColumns()
        values = row.getValues()
        valuesTemplate = ','.join(['%s'] * len(values))
        updates = row.getUpdates()
        queryParams = []

        query = 'INSERT INTO {} {} VALUES ({})'.format(table, columns, valuesTemplate) 
        queryParams = queryParams + values
        
        if overwrite:
            query += ' ON DUPLICATE KEY UPDATE '
            for update in updates:
                query += update[0] + '=%s,'
                queryParams.append(update[1])
            query = query.rstrip(',')

        query += ';'
        
        beforeHooks = self._getHooks(RowHook.PREPOSITION.BEFORE, RowHook.ACTION.WRITE, table)
        self._fireHooks(beforeHooks, [rowDict])

        result = self.query(query, queryParams, serializable=False)
        
        afterHooks = self._getHooks(RowHook.PREPOSITION.AFTER, RowHook.ACTION.WRITE, table)
        self._fireHooks(afterHooks, [rowDict])
        
        return result

    def deleteRows(self, table, constraints, entity=None, **kwargs):
        '''
        Deletes rows from the given table by column values.
        '''
        
        queryParams = []
        
        query = 'DELETE FROM {}'.format(table)
        
        builder = WhereClauseBuilder()

        query += builder.createWhereClause(constraints=constraints, queryParams=queryParams,
            entity=entity)
        
        limit = kwargs.get('limit', None)
        if limit is not None:
            query += ' LIMIT %s'
            queryParams.append(limit)

        query += ';'
        
        beforeHooks = self._getHooks(RowHook.PREPOSITION.BEFORE, RowHook.ACTION.DELETE, table)
        afterHooks = self._getHooks(RowHook.PREPOSITION.AFTER, RowHook.ACTION.DELETE, table)
        if beforeHooks or afterHooks:
            selectQuery = query.replace('DELETE', 'SELECT *')
            result = self.query(selectQuery, queryParams)
            rowDeletions = list(result)
            self._fireHooks(beforeHooks, rowDeletions)

        result = self.query(query, queryParams)
        
        if afterHooks:
            self._fireHooks(afterHooks, rowDeletions)
        
        return result
    
    def query(self, queryString, params=None, *, serializable=True):
        '''
        Queries the database and returns the cursor result.
        queryString: str
            An SQL query.
        params: dict
            Query params.
        '''
        if self.isClosed():
            self.open()
        
        try:
            cursor = self._database.cursor(pymysql.cursors.DictCursor)
            cursor.execute(queryString, params)
        except BaseException as e:
            if hasattr(cursor, '_last_executed'):
                lastQuery = cursor._last_executed
            else:
                lastQuery = None
            self._logger.error('{}\n\n{}\n\n{}'.format(queryString, params, str(lastQuery)))
            raise e
            
        if serializable:
            return map(MysqlClient.toJSONSerializable, cursor)
        
        return cursor

    def getRows(self, table, constraints, *, altTableSet=None, limit=None, offset=None,
        orderBy='Id ASC', columns=['*'], groupBy=None, serializable=True):
        '''
        Returns a list of rows from table where values are equal to those given by constraints.
        '''
        if (limit is not None and offset is None) or (limit is None and offset is not None):
            raise MysqlClient.QueryError('limit and offset must be used together.')
        
        queryParams = []

        query = 'SELECT {} FROM {}'.format(','.join(columns), table)
        
        builder = WhereClauseBuilder()
        query += builder.createWhereClause(constraints=constraints, queryParams=queryParams)

        if groupBy is not None:
            query += ' GROUP BY {}'.format(groupBy)

        if orderBy is not None:
            query += ' ORDER BY {}'.format(orderBy)

        if limit is not None and offset is not None:
            query += ' LIMIT %s OFFSET %s'
            queryParams.append(limit)
            queryParams.append(offset)

        return self.query(query, queryParams, serializable=serializable)

    def getRow(self, *args, **kwargs):
        '''
        Returns a single row, or None if now row is found.
        Has identical parameters to getRows(),
        '''

        result = self.getRows(*args, **kwargs)

        try:
            return list(result)[0]
        except IndexError:
            return None
    
    def addRowHook(self, hook):
        '''
        Add a row hook that will be fired as necessary.
        '''
        
        self._rowHooks.append(hook)
    
    def _getHooks(self, preposition, action, tableName=None):
        function = lambda hook: \
            preposition == hook.preposition \
            and action == hook.action \
            and (tableName is None or tableName == hook.tableName)
        return list(filter(function, self._rowHooks))
    
    def _fireHooks(self, hooks, rows):
        for hook in hooks:
            for row in rows:
                hook.callback(row) 
 