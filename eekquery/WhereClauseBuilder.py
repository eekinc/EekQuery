from eekquery.TrustedStringLiteral import TrustedStringLiteral

class WhereClauseBuilder:
    
    _WHERE_CLAUSE = ' WHERE '
    _EMPTY_WHERE_CLAUSE = ' WHERE TRUE '

    class InvalidInputException(RuntimeError):
        pass
    
    def createWhereClause(self, *, constraints, queryParams=[], entity=None):
        '''
        Constructs a WHERE clause.
        '''

        if isinstance(constraints, dict):
            return self.createWhereClauseFromDictionary(constraints, queryParams)
        else:
            return self.createWhereClauseFromConstraints(constraints, queryParams, entity)
    
    def createWhereClauseFromDictionary(self, dictionary, queryParameters):
        '''
        Constructs a WHERE clause from the supplied dictionary.
        '''
        
        if not dictionary:
            return self._EMPTY_WHERE_CLAUSE
        
        query = self._WHERE_CLAUSE
        
        i = 0
        for constraintKey, constraintValue in dictionary.items():
            if i > 0:
                query += ' AND '
            i += 1
            
            if isinstance(constraintValue, tuple):
                operator, operand = (x for x in constraintValue)
                if isinstance(operand, TrustedStringLiteral):
                    query += '{} {} {}'.format(constraintKey, operator, str(operand))
                else:
                    query += '{} {} %s'.format(constraintKey, operator)
                    queryParameters.append(operand)
            else:
                query += '{}=%s'.format(constraintKey)
                queryParameters.append(constraintValue)
                
        return query
    
    def createWhereClauseFromConstraints(self, constraints, queryParameters, entity=None):
        '''
        Constructs a WHERE clause from the supplied constraints.
        '''
        
        if not constraints:
            return self._EMPTY_WHERE_CLAUSE
        
        query = self._WHERE_CLAUSE
        i = 0
        
        for constraint in constraints:
            operator = constraint.comparisonOperator
            operand = constraint.operand
            columnName = constraint.targetColumnName

            if i > 0:
                query += ' AND '
            i += 1
                
            if operand is not None:
                if isinstance(operand, TrustedStringLiteral):
                    query += '{} {} {}'.format(columnName, operator, str(operand))
                else:
                    query += '{} {} %s'.format(columnName, operator)
                    queryParameters.append(operand)
            elif entity is not None:
                query += '{}{}%s'.format(columnName, operator)
                queryParameters.append(entity[constraint.sourceKeyName])
            else:
                raise WhereClauseBuilder.InvalidInputException(
                    'Either an operand or an entity must be provided.')
                
        return query
    
    
    
    
