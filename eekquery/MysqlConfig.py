'''
Config file containing credentials for rds mysql instance.
'''

from os import environ

class MysqlConfig:
    def __init__(self, username, password, name, endpoint, *, charset='latin1'):
        self.username = username
        self.password = password
        self.name = name
        self.endpoint = endpoint
        self.port = 3306
        self.charset = charset

TEST_CONFIG = MysqlConfig('root', environ.get('MYSQL_PASSWORD'), 'eekedout_test', '127.0.0.1')