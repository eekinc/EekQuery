from eekquery.Constraint import Constraint
from eekquery.TrustedStringLiteral import TrustedStringLiteral

class SyncHandler:

    def __init__(self, **kwargs):
        # Name of the table to write the updated data to.
        self._writeToTable = kwargs.get('writeToTable')
        
        # Columns to use to distinguish row sets.
        # Eg: If getRowsForObjectF returns a list of Packages for a Facility,
        # use deletionConstraints=['FacilityNumber']
        # Rows not returned from Metrc will be DELETED where these columns and values match.
        self._deletionConstraints = kwargs.get('deletionConstraints', None)
        
        # Function which returns a list of rows from Metrc for a given object.
        self._getRowsForObjectF = kwargs.get('getRowsForObjectF')
        
        # The primary key column for the writeToTable.
        self._keyColumn = kwargs.get('keyColumn', 'Id')
        
        self._database = kwargs.get('client')

    def _deleteWhereNotIn(self, obj, rows):
        '''
        Deletes rows where whereValues matches row values (by column)
        and row[keyCol] not in rows.
        '''
        rowIds = [row[self._keyColumn] for row in rows]
        rowIds = str(rowIds).replace('[', '(').replace(']', ')')
        
        constraints = list(self._deletionConstraints)
        
        if len(rows) > 0:
            constraint = Constraint(
                targetColumnName=self._keyColumn,
                comparisonOperator='NOT IN',
                operand=TrustedStringLiteral(rowIds)
            )
            constraints.append(constraint)
        
        self._database.deleteRows(self._writeToTable, constraints, entity=obj)

    def _sync(self, obj, rows):
        for row in rows:
            self._database.writeRow(row, self._writeToTable)

        self._deleteWhereNotIn(obj, rows)

    def syncAll(self, objects):
        '''
        Syncs all rows per-object.
        '''

        for obj in objects:
            # TODO(jdsutton): Try to make this a stream instead of loading everything.
            # Ensure we have all the data and can walk over it more than once.
            rows = self._getRowsForObjectF(obj)

            if rows is None:
                continue

            self._sync(obj, list(rows))