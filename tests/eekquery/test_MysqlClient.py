from datetime import datetime
from eekquery.MysqlClient import MysqlClient
from eekquery.MysqlConfig import TEST_CONFIG
from eekquery.RowHook import RowHook
import json
from pymysql.err import IntegrityError
import unittest

class test_MysqlClient(unittest.TestCase):
    
    def setUp(self):
        global rds

        rds = MysqlClient(config=TEST_CONFIG)
        rds.query('DROP TABLE IF EXISTS Packages;')
        rds.query('''
            CREATE TABLE Packages(Id INT(11) NOT NULL AUTO_INCREMENT, FacilityNumber VARCHAR(45),
            ProductName VARCHAR(45), PRIMARY KEY (Id));
        ''')
        rds.query('''
            INSERT INTO Packages (Id, FacilityNumber, ProductName) VALUES
            (1, 'FacilityNumber0', 'cool product'),
            (2, 'FacilityNumber0', 'cool product'),
            (3, 'FacilityNumber0', 'cool product'),
            (4, 'FacilityNumber0', 'cool product'),
            (5, 'FacilityNumber1', 'cool product'),
            (6, 'FacilityNumber1', 'cool product'),
            (7, 'FacilityNumber1', 'cool product');
        ''')

    def test_toJSONSerializable(self):
        packages = rds.query('SELECT * FROM Packages;')
        packages = map(MysqlClient.toJSONSerializable, packages)

        jsonSerialized = json.dumps(list(packages))

        class BadDateTime(datetime):

            def timestamp(self):
                raise ValueError()

        packages = [{
            'date': BadDateTime(1, 1, 1)
        }]
        packages = map(MysqlClient.toJSONSerializable, packages)
        jsonSerialized = json.dumps(list(packages))

    def test_writeRow(self):
        packageRow = {
            'Id': 1234567,
            'FacilityNumber': 'FakeFacilityNumber, LLC',
            'ProductName': 'Cool Product'
        }

        result = rds.writeRow(packageRow, 'Packages')
        self.assertIsNotNone(result.lastrowid)

        def alreadyExists():
            rds.writeRow(packageRow, 'Packages', overwrite=False)

        self.assertRaises(IntegrityError, alreadyExists)

        packages = rds.query('SELECT * FROM Packages WHERE FacilityNumber="{}";'.format('FakeFacilityNumber, LLC'))
        packages = list(packages)
        self.assertEqual(len(packages), 1)

        package = packages[0]
        self.assertEqual(package['Id'], 1234567)
        self.assertEqual(package['FacilityNumber'], 'FakeFacilityNumber, LLC')
        self.assertEqual(package['ProductName'], 'Cool Product')

        packageRow['ProductName'] = 'Cool Product 2.0'
        rds.writeRow(packageRow, 'Packages')

        packages = rds.query('SELECT * FROM Packages WHERE FacilityNumber="{}";'.format('FakeFacilityNumber, LLC'))
        packages = list(packages)
        self.assertEqual(len(packages), 1)

        package = packages[0]
        self.assertEqual(package['Id'], 1234567)
        self.assertEqual(package['FacilityNumber'], 'FakeFacilityNumber, LLC')
        self.assertEqual(package['ProductName'], 'Cool Product 2.0')

    def test_beforeAndAfterRowWritten(self):
        # Reset.
        rds._rowHooks = []
        
        tableName = 'Packages'
        packageRow = {
            'Id': 756,
            'FacilityNumber': 'F00',
            'ProductName': 'B4R'
        }
        self.i = 0
        
        def before(row):
            self.assertEqual(packageRow, row)
            # Ensure that the data has NOT already been written,
            row = rds.getRow(tableName, {'Id': packageRow['Id']})
            self.assertIsNone(row)
            # Increment our counter.
            self.i += 1
        rds.addRowHook(RowHook(RowHook.PREPOSITION.BEFORE, RowHook.ACTION.WRITE, tableName, before))

        def after(row):
            self.assertEqual(packageRow, row)
            # Ensure that `after` is after `before`.
            self.assertEqual(self.i, 1)
        rds.addRowHook(RowHook(RowHook.PREPOSITION.AFTER, RowHook.ACTION.WRITE, tableName, after))

        result = rds.writeRow(packageRow, tableName)
        self.assertIsNotNone(result.lastrowid)

    def test_getRows(self):
        rows = list(rds.getRows('Packages', {
            'FacilityNumber': 'FacilityNumber0'    
        }))

        rows.sort(key=lambda x: x['Id'])

        self.assertEqual(len(rows), 4)

        self.assertEqual(rows[0]['Id'], 1)
        self.assertEqual(rows[0]['FacilityNumber'], 'FacilityNumber0')

        rds.getRows('Packages', {
            'FacilityNumber': ('LIKE', 'FacilityNumber%')    
        })

    def test_deleteRows(self):
        items = rds.deleteRows('Packages', {
        'FacilityNumber': 'FacilityNumber0'
        })

        items = rds.getRows('Packages', {
            'FacilityNumber': 'FacilityNumber0'
        })
        self.assertEqual(len(list(items)), 0)
        
    def test_beforeAndAfterRowDeleted(self):
        rds._rowHooks = []
        
        tableName = 'Packages'
        packageRow = {
            'Id': 1024,
            'FacilityNumber': 'F00',
            'ProductName': 'B4R'
        }
        self.i = 0
        
        # We must have a record to delete.
        result = rds.writeRow(packageRow, tableName)
        self.assertIsNotNone(result.lastrowid)
        
        def before(row):
            self.assertEqual(packageRow, row)
            # Ensure that the data has NOT already been deleted,
            row = rds.getRow(tableName, {'Id': packageRow['Id']})
            self.assertIsNotNone(row)
            # Increment our counter.
            self.i += 1
        rds.addRowHook(RowHook(RowHook.PREPOSITION.BEFORE, RowHook.ACTION.DELETE, tableName, before))

        def after(row):
            self.assertEqual(packageRow, row)
            # Ensure that `after` is after `before`.
            self.assertEqual(self.i, 1)
        rds.addRowHook(RowHook(RowHook.PREPOSITION.AFTER, RowHook.ACTION.DELETE, tableName, after))

        rds.deleteRows(tableName, {'Id': packageRow['Id']})


if __name__ == '__main__':
    unittest.main()
