CREATE_DATABASE_SQL = "CREATE DATABASE IF NOT EXISTS eekedout_test; USE eekedout_test;"

default:
	echo "Nothing to do"

install:
	apt-get update
	python --version

	@echo "mysql-server mysql-server/root_password password $(MYSQL_PASSWORD)" | debconf-set-selections
	@echo "mysql-server mysql-server/root_password_again password $(MYSQL_PASSWORD)" | debconf-set-selections
	apt-get -y install mysql-server python3-setuptools
	pip install -U setuptools
	git submodule update --init --recursive
	pip install .

	@/etc/init.d/mysql start
login:
	@$(shell aws ecr get-login)

test: create_database
	nosetests --nocapture --with-coverage --cover-package=eekquery
	coverage report -m --skip-covered

create_database:
	echo $(CREATE_DATABASE_SQL) | mysql --host=localhost -uroot -p$(MYSQL_PASSWORD)

clean:
	rm -rf build
	rm -rf *.egg-info

docs:
	mkdir -p documentation
	rm -f documentation/*.html
	python3 -m pydoc -w ./ > documentation/log.txt
	mv *.html documentation
	rm *.pyc
